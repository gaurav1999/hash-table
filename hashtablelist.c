/*
 * Glib core's  Hash Table with multiple values for a single key example code. With good 
 * memory management and error checking.
 */

#include "hashtablelist-config.h"
#include <glib.h>
#include <glib/gi18n.h>
#include <stdio.h>
#include <locale.h>

void print_array (gpointer value, gpointer user_data)
{
   if (value)
   {
      gchar *val = value;
      g_print (" %s", val);
   }
}

void print_element (gpointer key,gpointer value,gpointer user_data)
{
    if(key && value)
   {
      gchar *str = key;
      GPtrArray *valuelist = (GPtrArray *)value;
      g_print ("%s =", str);

      g_ptr_array_foreach (valuelist, print_array, NULL);
      g_print ("\n");
   }
}

void print_menu (GHashTable *table, gboolean print_get_keys)
{
   g_print (_("Menu:\n"));
   gboolean table_size;

   table_size = g_hash_table_size (table) > 0;
   if (table_size)
   {
      g_print (_("1. View Table\n"));
   }
   g_print (_("2. Add Key Value Pairs\n"));
   if (table_size && print_get_keys)
   {
      g_print (_("3. Get Values of Key\n"));
   }
   printf(_("4. Exit\n"));
}

static void
destroy_elements (gpointer element)
{
  g_ptr_array_free (element, TRUE);
}


gint main (gint argc, gchar **argv)
{
   int choice;
   GHashTable *table;

   /* Set up gettext translations */
   setlocale (LC_ALL, "");
   bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
   bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
   textdomain (GETTEXT_PACKAGE);

   table = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, destroy_elements);
   g_print (_("Welcome to Hash Table Function\n"));
   print_menu (table, FALSE);
   while (scanf ("%d", &choice))
   {
      if (choice == 1)
      {
         g_print (_("Printing values:\n"));
         g_hash_table_foreach (table, print_element, NULL);
      }
      else if (choice == 2)
      {
         g_print (_("Enter Key\n"));
         gchar key[50] , value[50];
         scanf ("%s", key);
         printf (_("Enter Value\n"));
         scanf ("%s",value);
         if (g_hash_table_lookup (table, (gpointer)key))
         {
            GPtrArray *valuelist = g_hash_table_lookup(table, key);
            g_ptr_array_add (valuelist, (gpointer)g_strdup(value));
         }
         else
         {
            GPtrArray *valuelist = g_ptr_array_new_with_free_func (g_free);
            g_ptr_array_add (valuelist, (gpointer)g_strdup (value));
            g_hash_table_insert (table, (gpointer)g_strdup (key), (gpointer)valuelist);
         }
      }
      else if (choice == 3)
      {
         g_print (_("Enter Key\n"));
         gchar key[50] ;
         scanf("%s", key);
         if (g_hash_table_lookup (table, key))
         {
            GPtrArray *valuelist = g_hash_table_lookup(table, key);
            g_print ("%s =", key);
            g_ptr_array_foreach (valuelist, print_array, NULL);
            g_print ("\n");
         }
         else
         {
            g_print (_("key %s not found\n"), key);
         }
      }
      else if (choice == 4)
      {
         break;
      }
      else
      {
         g_print (_("Unknow option\n"));
      }
      print_menu (table, TRUE);
   }
   g_hash_table_remove_all (table);
   g_hash_table_unref (table);
}

