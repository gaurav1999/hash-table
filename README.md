# glib hashtable example

## Compiling

    meson build
    ninja -C build

## Running

    ./build/hashtablelist

## Output example

```
./build/hashtablelist     
Welcome to Hash Table Function
Menu:
2. Add Key Value Pairs
4. Exit
2
Enter Key
a
Enter Value
b
Menu:
1. View Table
2. Add Key Value Pairs
3. Get Values of Key
4. Exit
2
Enter Key
b
Enter Value
c
Menu:
1. View Table
2. Add Key Value Pairs
3. Get Values of Key
4. Exit
1
Printing values:
a = b
b = c
Menu:
1. View Table
2. Add Key Value Pairs
3. Get Values of Key
4. Exit
3
Enter Key
a
a = b
Menu:
1. View Table
2. Add Key Value Pairs
3. Get Values of Key
4. Exit
3
Enter Key
c
key c not found
Menu:
1. View Table
2. Add Key Value Pairs
3. Get Values of Key
4. Exit
4
```
